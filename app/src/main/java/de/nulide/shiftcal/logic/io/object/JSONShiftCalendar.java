package de.nulide.shiftcal.logic.io.object;

import java.util.ArrayList;
import java.util.List;

public class JSONShiftCalendar {

    private List<JSONWorkDay> calendar;
    private List<JSONShift> shifts;

    private List<JSONMonthNote> notes;

    public JSONShiftCalendar() {
        calendar = new ArrayList<>();
        shifts = new ArrayList<>();
        notes = new ArrayList<>();
    }

    public JSONShiftCalendar(List<JSONWorkDay> calendar, List<JSONShift> shifts, List<JSONMonthNote> notes) {
        this.calendar = calendar;
        this.shifts = shifts;
        this.notes = notes;
    }

    public List<JSONWorkDay> getCalendar() {
        return calendar;
    }

    public void setCalendar(List<JSONWorkDay> calendar) {
        this.calendar = calendar;
    }

    public List<JSONShift> getShifts() {
        return shifts;
    }

    public void setShifts(List<JSONShift> shifts) {
        this.shifts = shifts;
    }

    public List<JSONMonthNote> getNotes() {
        return notes;
    }

    public void setNotes(List<JSONMonthNote> notes) {
        this.notes = notes;
    }
}
