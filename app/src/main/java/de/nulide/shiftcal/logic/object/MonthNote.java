package de.nulide.shiftcal.logic.object;

public class MonthNote {
    private int year;
    private int month;
    private String note;

    public MonthNote(int year, int month, String note) {
        this.year = year;
        this.month = month;
        this.note = note;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
